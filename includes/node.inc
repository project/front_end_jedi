<?php
/**
 * @file
 * Theme hooks and overrides for node module.
 */

/**
 * Implements hook_preprocess_node().
 */
function front_end_jedi_preprocess_node(&$variables) {
  $view_mode = $variables['view_mode'];
  $node = $variables['node'];

  $suggestions = [
    'node__' . str_replace('-', '_', $view_mode),
    'node__' . $node->type . '__' . str_replace('-', '_', $view_mode),
  ];

  $variables = front_end_jedi_add_suggestions($variables, $suggestions);
}
