<?php
/**
 * @file
 * Theme-independent logic.
 */

/**
 * Copy all rendered content to be accessible by direct key name.
 *
 * @param array $variables
 *   Variables.
 *
 * @return array
 *   Processed variables.
 */
function front_end_jedi_prepare_layout_variables(array $variables) {
  foreach ($variables['layout']['regions'] as $key => $title) {
    $variables[$key] = isset($variables['content'][$key]) ? $variables['content'][$key] : '';
  }

  return $variables;
}

/**
 * Helper to call render on specified elements.
 *
 * @param array $variables
 *   Variables.
 * @param array $render_elements
 *   Array of elements which should be rendered.
 *
 * @return array
 *   Updated variables (with rendered elements).
 */
function front_end_jedi_prepare_rendered_vars(array $variables, array $render_elements) {
  foreach ($render_elements as $name) {
    $variables['rendered_' . $name] = render($variables[$name]);
  }

  return $variables;
}

/**
 * Render single field from content build array.
 *
 * @param array $variables
 *   Variables.
 * @param string $field_name
 *   Field to be rendered.
 *
 * @return string
 *   Rendered field or empty string if failed.
 */
function front_end_jedi_render_content_var(array $variables, $field_name) {
  return isset($variables['content'][$field_name])
    ? render($variables['content'][$field_name])
    : '';
}

/**
 * Helper to call render on specified elements from content.
 *
 * @param array $variables
 *   Variables.
 * @param array $fields
 *   Array of fields to be rendered.
 *
 * @return array
 *   Updated variables array.
 */
function front_end_jedi_prepare_rendered_content_vars(array $variables, array $fields) {
  foreach ($fields as $source => $target) {
    $variables[$target] = front_end_jedi_render_content_var($variables, $source);
  }

  return $variables;
}

/**
 * Add suggestions and appropriates preprocesses to variables.
 *
 * @param array $variables
 *   Variables array.
 * @param array $suggestions
 *   Suggestions which should be added.
 *
 * @return array
 *   Variables with new suggestions.
 */
function front_end_jedi_add_suggestions(array $variables, array $suggestions) {
  global $theme;
  foreach ($suggestions as $suggestion) {
    $variables['theme_hook_suggestions'][] = $suggestion;
    $preprocess = $theme . '_preprocess_' . $suggestion;
    if (function_exists($preprocess)) {
      $preprocess($variables);
    }
  }

  return $variables;
}
