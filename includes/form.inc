<?php

/**
 * @file
 * Theme hooks related to forms.
 */

/**
 * Override of theme_form_element().
 *
 * Here are many improvements:
 *  * You can add 'id' and other attributes to wrapper via #wrapper_id and
 *    #wrapper_attributes flags of element. Also can set classes to wrapper
 *    via #wrapper_classes flag of element.
 *  * Adds form-item class to wrapper. You can use this class as additional way
 *    to write short and uniq selector for your element.
 *  * Adds form-disabled class to disabled elements. You can use this for
 *    facilitate cross browser styling.
 *  * You can fully hide title. Just unset #title and #title_display.
 *  * You can output suffix or prefix for the element. See #field_prefix and
 *    #field_suffix.
 *  * You can disable description via #ignore_description.
 *  * Also please check all title_display to have clear vision how this theme
 *    works.
 *
 * @param array $variables
 *   Input variables.
 *
 * @see theme_form_element()
 *
 * @return string
 *   Output string of HTML.
 */
function front_end_jedi_form_element(array $variables) {
  $element = &$variables['element'];

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += ['#title_display' => 'before'];

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // @todo Implement it via '#wrapper_attributes'.
  elseif (!empty($element['#wrapper_id'])) {
    $attributes['id'] = $element['#wrapper_id'];
  }

  $attributes['class'] = [];
  // Add custom attributes to wrapper.
  // Note, we do this before classes generation to merge them all.
  if (!empty($element['#wrapper_attributes'])) {
    foreach ($element['#wrapper_attributes'] as $attribute => $value) {
      $attributes[$attribute] = $value;
    }
  }

  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'][] = 'form-item';
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $replacement = [' ' => '-', '_' => '-', '[' => '-', ']' => ''];
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], $replacement);
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }

  // Add custom wrapper classes from element definition.
  if (!empty($element['#wrapper_classes'])) {
    $attributes['class'] = array_merge($attributes['class'], $element['#wrapper_classes']);
  }

  // If #title is not set and #title_display is not set too,
  // we don't display any label or required marker. Otherwise handle it via
  // #title_display property.
  if (!isset($element['#title']) && !isset($element['#title_display'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<div class="field-prefix">' . $element['#field_prefix'] . '</div> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <div class="field-suffix">' . $element['#field_suffix'] . '</div>' : '';

  if (!empty($element['#required']) && $element['#title_display'] == 'invisible') {
    $element['#title_display'] = 'required_invisible';
  }

  $description = '';
  if (!empty($element['#description']) && empty($element['#ignore_description'])) {
    $description = '<div class="description form-item__description">' . $element['#description'] . "</div>\n";
  }

  $visibile_label_types = ['before', 'after'];
  if (in_array($element['#title_display'], $visibile_label_types)) {
    $label = theme('form_element_label', $variables);
    if ($description) {
      $label .= $description;
    }
  }

  $rendered_element = $prefix . $element['#children'] . $suffix;
  $element_error = !empty($element['#ife_error_position']) && $element['#ife_error_position'] == IFE_POSITION_INLINE_MESSAGE_CUSTOM;
  // Handle file errors (@see file_managed_file_validate()).
  $file_element_error = !empty($element['#type']) && $element['#type'] == 'managed_file'
    && !empty($element['upload']['#ife_error_position']) && $element['upload']['#ife_error_position'] == IFE_POSITION_INLINE_MESSAGE_CUSTOM;
  if ($element_error || $file_element_error) {
    $element_copy = !empty($element['upload']) ? $element['upload'] : $element;
    // Put rendered string as a child to put an error after suffix.
    $element_copy['#children'] = $rendered_element;
    // We want to appear error right after <input> element.
    $element_copy['#ife_error_position'] = IFE_POSITION_INLINE_MESSAGE_AFTER;
    $rendered_element = theme('ife_form_element', ['element' => $element_copy]);
    $attributes['class'][] = 'form-item--has-errors';
  }

  // For some element types we want to provide extra label to style them.
  if (isset($element['#type'])) {
    switch ($element['#type']) {
      case 'radio':
      case 'checkbox':
        $label_attrs = array(
          'class' => array('form-item__data-empty-label'),
        );
        // In some cases element id can miss.
        // See details in https://www.drupal.org/node/2853388.
        if (isset($element['#id'])) {
          $label_attrs['for'] = $element['#id'];
        }
        $rendered_element .= '<label' . drupal_attributes($label_attrs) . '></label>';
        break;
    }
  }

  $output = '';
  switch ($element['#title_display']) {
    case 'before':
      if (!empty($label)) {
        $output .= '<div class="form-item__info">' . $label . '</div>';
      }
      elseif (!empty($element['#show_info_block']) && $element['#show_info_block']) {
        $output .= '<div class="form-item__info"></div>';
      }
      $output .= '<div class="form-item__data">' . ' ' . $rendered_element . "\n" . '</div>';
      break;

    case 'after':
      $output .= '<div class="form-item__data">' . ' ' . $rendered_element . "\n" . '</div>';
      $output .= !empty($label) ? '<div class="form-item__info">' . $label . '</div>' : '';
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= '<div class="form-item__data">' . ' ' . $rendered_element . "\n" . '</div>';
      $output .= $description;
      break;

    case 'invisible':
    case 'required_invisible':
      $output .= '<div class="form-item__data">' . ' ' . $rendered_element . "\n" . '</div>';
      break;
  }

  $output = '<div' . drupal_attributes($attributes) . '>' . "\n" . $output . "</div>\n";

  return $output;
}
