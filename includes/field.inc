<?php

/**
 * @file
 * Theme hooks and overrides for field core module.
 */

/**
 * Override of theme_field().
 *
 * @param array $variables
 *   Variables.
 *
 * @return string
 *   Built HTML.
 */
function front_end_jedi_field(array $variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden'] && $variables['label']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
  }

  // Render the items.
  foreach ($variables['items'] as $delta => $item) {
    $output .= drupal_render($item);
  }

  return $output;
}

/**
 * Implements hook_preprocess_field().
 */
function front_end_jedi_preprocess_field(&$variables, $hook) {
  $element = $variables['element'];

  // Pass element classes to wrapper.
  if (isset($element['#classes_array'])) {
    $variables['classes_array'] = array_merge($variables['classes_array'], $element['#classes_array']);
  }
}

/**
 * Implements hook_field_attach_view_alter().
 *
 * Provides alt and title for images if not set.
 */
function front_end_jedi_field_attach_view_alter(&$output, $context) {
  if (!empty($context['entity_type']) && !empty($context['entity'])) {
    foreach (element_children($output) as $child_key) {
      // Affect only image fields.
      $image_field_types = module_invoke_all('image_field_types');
      if (in_array($output[$child_key]['#field_type'], $image_field_types)) {
        foreach (element_children($output[$child_key]) as $sub_child_key) {
          // And only with direct image formatter.
          if (isset($output[$child_key][$sub_child_key]['#theme'])
            && $output[$child_key][$sub_child_key]['#theme'] == 'image_formatter'
            && empty($output[$child_key][$sub_child_key]['#item']['alt'])
            && empty($output[$child_key][$sub_child_key]['#item']['title'])) {
            $entity_label = entity_label($context['entity_type'], $context['entity']);
            $output[$child_key][$sub_child_key]['#item']['alt'] = $entity_label;
            $output[$child_key][$sub_child_key]['#item']['title'] = $entity_label;
          }
        }
      }
    }
  }
}

/**
 * Implements hook_image_field_types().
 */
function front_end_jedi_image_field_types() {
  $items['image'] = 'image';

  $items['imagefield_crop'] = 'imagefield_crop';

  return $items;
}
