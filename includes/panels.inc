<?php
/**
 * @file
 *
 * Theme hooks and overrides for panels module.
 */

/**
 * Implements hook_preprocess_panels_pane().
 */
function front_end_jedi_preprocess_panels_pane(&$variables) {
  $variables = front_end_jedi_prepare_rendered_vars($variables, [
    'title_prefix',
    'title_suffix',
    'content'
  ]);

  $pane = $variables['pane'];
  $type_source = $pane->subtype ? $pane->subtype : $pane->type;
  $block_id = $type_source ? 'block-' . ctools_cleanstring($type_source, array('lower case' => TRUE)) : '';
  $variables['content_class'] = $block_id . '__content';
  $original_classes = $variables['classes_array'];
  $variables['classes_array'] = [$block_id];
  if (in_array('contextual-links-region', $original_classes)) {
    $variables['classes_array'][] = 'contextual-links-region';
  }
  if (!empty($pane->css['css_class'])) {
    $variables['classes_array'][] = $pane->css['css_class'];
  }
  $variables['title_attributes_array']['class'] = [$block_id . '__title'];

  if ($pane->type === 'page_content' && $pane->subtype === 'page_content') {
    $extra_classes = front_end_jedi_page_content_pane_class();
    if ($extra_classes) {
      $extra_classes = implode(' ', $extra_classes);
      if (empty($variables['content_class'])) {
        $variables['content_class'] = $extra_classes;
      }
      else {
        $variables['content_class'] .= ' ' . $extra_classes;
      }
    }
  }
}

/**
 * Override of theme_panels_default_style_render_region().
 *
 * Wipe separator from default style plugin (it's hard to set up another
 * everywhere).
 *
 * @param array $variables
 *   Variables.
 *
 * @return string
 *   Built HTML.
 */
function front_end_jedi_panels_default_style_render_region(array $variables) {
  if (variable_get('theme_debug', FALSE)) {
    $output = '';
    foreach ($variables['panes'] as $pid => $pane_output) {
      $pane = $variables['display']->content[$pid];
      $output .= $pane_output;
      $output .= '<!-- Region:' . $variables['region_id'] . ' Pane:' . ($pane->type == $pane->subtype ? $pane->type : $pane->type . ':' . $pane->subtype) . ' -->';
    }

    // Don't return empty regions with HTML comment.
    // It will lead to empty wrappers which could brake markup.
    // @todo We need to find a way to replace this feature to debug empty regions.
    if ($output) {
      $output = $output . '<!-- End region:' . $variables['region_id'] . ' -->';
    }

    return $output;
  }
  else {
    // Wipe panel separator.
    return implode('', $variables['panes']);
  }
}

/**
 * Static setter/getter for context based extra-classes of page_content pane.
 *
 * @param string|null $class
 *   New class to be set up.
 *
 * @return array
 *   Array of currently stored classes.
 */
function front_end_jedi_page_content_pane_class($class = NULL) {
  $classes = &drupal_static(__FUNCTION__, []);

  if (!is_null($class) && !in_array($class, $classes)) {
    $classes[] = $class;
  }

  return $classes;
}
