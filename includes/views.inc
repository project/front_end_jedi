<?php
/**
 * @file
 *
 * Theme hooks and overrides for views module.
 */

/**
 * Implements hook_preprocess_views_view().
 */
function front_end_jedi_preprocess_views_view(&$variables) {
  $variables = front_end_jedi_prepare_rendered_vars($variables, [
    'title_prefix',
    'title_suffix'
  ]);
}
