<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @var array $rows
 * @var array $classes_array
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if (!empty($classes_array[$id])) { print ' class="' . $classes_array[$id] . '"';  } ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
