<?php

/**
 * @file
 * Intermediate logic between real preprocesses and custom templates.
 */

require_once __DIR__ . '/includes/theme-helpers.inc';
require_once __DIR__ . '/includes/field.inc';
require_once __DIR__ . '/includes/node.inc';
require_once __DIR__ . '/includes/form.inc';
require_once __DIR__ . '/includes/panels.inc';
require_once __DIR__ . '/includes/views.inc';

/**
 * Implements hook_preprocess_html().
 */
function front_end_jedi_preprocess_html(&$variables) {
  $variables['lang_code'] = $variables['language']->language;
  $variables['lang_dir'] = $variables['language']->dir;
  $variables['skip_to_main_text'] = t('Skip to main content');
}

/**
 * Get list of fonts defined within current theme info file.
 *
 * @return array
 *   Array of paths to fonts.
 */
function front_end_jedi_get_theme_fonts() {
  // Handle custom fonts option for themes.
  $current_theme = variable_get('theme_default');
  $themes = list_themes();
  $theme_info = $themes[$current_theme];

  return !empty($theme_info->info['fonts']) ? $theme_info->info['fonts'] : [];
}

/**
 * Implements hook_css_alter().
 */
function front_end_jedi_css_alter(&$css) {
  $disallowed_css = [
    'modules/system/system.menus.css' => FALSE,
    'modules/system/system.messages.css' => FALSE,
    'modules/system/system.theme.css' => FALSE,
    'modules/system/system.base.css' => FALSE,
    'modules/user/user.css' => FALSE,
    'modules/file/file.css' => FALSE,
    'modules/image/image.css' => FALSE,
    'modules/node/node.css' => FALSE,
    'modules/field/theme/field.css' => FALSE,
  ];

  $css = array_diff_key($css, $disallowed_css);

  // Add theme-defined fonts at the beginning of all CSS.
  $font_css = [];
  foreach (front_end_jedi_get_theme_fonts() as $font) {
    $font_css[$font] = [
      'type' => 'external',
      'media' => 'all',
      'data' => $font,
      'group' => CSS_SYSTEM,
      'weight' => -100,
      'every_page' => FALSE,
      'preprocess' => TRUE,
      'browsers' => array(),
    ];
  }
  $css = array_merge($font_css, $css);
}

/**
 * Implements hook_ckeditor_settings_alter().
 */
function front_end_jedi_ckeditor_settings_alter(&$settings, $conf) {
  // Do nothing if we should not add theme styles.
  if ($conf['css_mode'] !== 'theme') {
    return;
  }

  // We want to add all fonts as CSS to CKEditor iframe.
  if ($fonts = front_end_jedi_get_theme_fonts()) {
    $settings['contentsCss'] = array_merge($fonts, $settings['contentsCss']);
  }
}
